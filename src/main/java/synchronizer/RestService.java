package synchronizer;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import synchronizer.entities.BpmnTask;
import synchronizer.entities.ExternalReference;
import synchronizer.entities.redmine.RedmineIssue;

@RestController
public class RestService {

	private static final Logger logger = LoggerFactory.getLogger(RestService.class);

	@Value("${TELEGRAM_CHAT_ID}")
	private String TELEGRAM_CHAT_ID;

	@Value("${TELEGRAM_API_TOKEN}")
	private String TELEGRAM_API_TOKEN;

	@Value("${MAIL_RECEIVER}")
	private String MAIL_RECEIVER;

	@Autowired
	private TaskUtil taskUtil;

	@Autowired
	JavaMailSender javaMailSender;

	@Autowired
	private ExternalReferenceRepository repository;

	@RequestMapping(value = "/redmine", method = RequestMethod.POST, produces = "application/json")
	public void process(@RequestBody String payload) throws Exception {

		RedmineIssue issue = taskUtil.deserializeRedmineIssue(payload);
		long issueId = issue.getId();
		if (issue.getStatus().getName().equals("Resolved")) {
			ExternalReference ref = repository.findByIssueId(issueId);
			String taskId = ref.getTaskId();
			// Send the callback to the process engine.
			String camundaUrl = ref.getCamundaUrl();
			URI url = taskUtil.generateCamundaTaskCompleteUrl(taskId, camundaUrl);
			logger.info("Issue with id: " + issueId + " has been completed!");
			String bodyJSON = "{\"workerId\":\"" + ref.getTaskWorkerId() + "\"}";
			sendIssue(bodyJSON, url, "BPMN Engine");
			logger.info("BPMN Task with id: " + taskId + " has been completed!");
			// remove reference entry from database because it's no longer needed
			repository.delete(ref);
		} else {
			logger.info("Issue with id: " + issueId + " has not yet been resolved.The issue's current status is: "
					+ issue.getStatus().getName() + "! ");
			return;
		}
		// TODO else?
	}

	@Retryable(value = { RestClientException.class }, maxAttempts = 5, backoff = @Backoff(delay = 5000))
	public String sendIssue(String bodyJSON, URI url, String receiver)
			throws JsonProcessingException, InterruptedException {
		logger.info("Attempting to send data to " + url.toString());
		String result = "";
		final RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.set("content-type", "application/json;charset=utf-8");

		HttpEntity<String> entity = new HttpEntity<String>(bodyJSON, headers);

		result = restTemplate.postForObject(url, entity, String.class);
		logger.info("Data sent succefully!");
		return result;
	}

	@Recover
	public String recover(RestClientException t, String issueJSON, URI url, String receiver)
			throws JsonParseException, JsonMappingException, IOException {
		// TODO Notify BPMN
		ObjectMapper mapper = new ObjectMapper();
		BpmnTask task = mapper.readValue(issueJSON, BpmnTask.class);
		logger.warn(receiver + " is unreachable!");

		String text = "Issue with subject: " + task.getSubject() + " and descripton: " + task.getDescription()
				+ ", for user with id: " + task.getAssigneeId() + " with Due Date set to: " + task.getDueDate()
				+ " and launched by process with id: " + task.getProcessId() + " has not been sent to " + receiver
				+ " due to connectivity problems after 4 attempts.";

		logger.info("Issue data will be send to Telegram channel!");
		sendPendingIssueToTelegram(text);

		// sender and receiver identifications should be specified in
		// application.properties
		if (!MAIL_RECEIVER.isEmpty()) {
			logger.info("Issue data will be send to the person responsible via mail!");
			sendSimpleMessage(MAIL_RECEIVER, "Issue not sended to " + receiver, text);
		}

		// TODO send notification to Pidgin

		return "";
	}

	public String getFromBpmnEngine(URI url) throws JsonProcessingException, InterruptedException {
		logger.info("Attempting to get data from " + url.toString());
		String result = "";
		final RestTemplate restTemplate = new RestTemplate();
		result = restTemplate.getForObject(url, String.class);
		logger.info("Data sent succefully!");
		return result;
	}

	public byte[] getImageFromBpmnEngine(URI url) throws JsonProcessingException, InterruptedException {
		logger.info("Attempting to get image data from " + url.toString());
		byte[] result;
		final RestTemplate restTemplate = new RestTemplate();
		result = restTemplate.getForObject(url, byte[].class);
		logger.info("Data sent succefully!");
		return result;
	}

	public void sendPendingIssueToTelegram(String text) {
		logger.info("Attempting to send issue to Telegram");
		String result = "";
		String apiToken = TELEGRAM_API_TOKEN;
		String chatId = TELEGRAM_CHAT_ID;
		final RestTemplate restTemplate = new RestTemplate();
		String url = "https://api.telegram.org/bot" + apiToken + "/sendMessage";
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", "application/json");
		headers.set("content-type", "application/json;charset=utf-8");

		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("chat_id", chatId)
				.queryParam("text", text);
		HttpEntity<String> entity = new HttpEntity<String>(text, headers);

		result = restTemplate.postForObject(builder.build().encode().toUri(), entity, String.class);
		logger.info("Telegram bot responded with: " + result);

	}

	public void sendSimpleMessage(String to, String subject, String text) {
		SimpleMailMessage mail = new SimpleMailMessage();

		mail.setTo(to);
		mail.setSubject(subject);
		mail.setText(text);

		logger.info("Sending...");

		javaMailSender.send(mail);

		logger.info("Done!");
	}

	@RequestMapping(value = "/images/{imageName}", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
	public void getImage(HttpServletResponse response, @PathVariable("imageName") String imageName) throws IOException {
		BufferedImage image = getImage("/var/www/img/" + imageName);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "png", baos);
		byte[] bytes = baos.toByteArray();

		response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		StreamUtils.copy(bytes, response.getOutputStream());
	}

	private BufferedImage getImage(String filename) {
		BufferedImage img = null;
		try {
			img = ImageIO.read(new File(filename));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}
}
