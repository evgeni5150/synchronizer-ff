package synchronizer.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class ExternalReference {
	 @Id
	 private String id;
	 
	 public String executionId;
	 public String taskId;
	 public String taskWorkerId;
	 public String camundaUrl;
	 
	public String getCamundaUrl() {
		return camundaUrl;
	}

	public void setCamundaUrl(String camundaUrl) {
		this.camundaUrl = camundaUrl;
	}

	public String getTaskWorkerId() {
		return taskWorkerId;
	}

	public void setTaskWorkerId(String taskWorkerId) {
		this.taskWorkerId = taskWorkerId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public long issueId;

	public String getId() {
		return id;
	}
	
    public ExternalReference() {}


	public void setId(String id) {
		this.id = id;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public long getIssueId() {
		return issueId;
	}

	public void setIssueId(long issueId) {
		this.issueId = issueId;
	}
	 
}