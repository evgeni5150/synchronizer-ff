package synchronizer.entities.redmine;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Notes implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("people")
	private List<People> people;

	@JsonProperty("people")
	public List<People> getPeople() {
		return people;
	}
	
	@JsonProperty("people")
	public void setPeople(List<People> people) {
		this.people = people;
	}
	
}
