package synchronizer.entities.redmine;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import synchronizer.entities.Tracker;

import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

@JsonTypeName("issue")
@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = JsonTypeInfo.Id.NAME)
public class RedmineIssue {

	@JsonProperty("id")
	private long id;
	@JsonProperty("project")
	private Project project;
	private Tracker tracker;
	@JsonProperty("status")
	private Status status;
	@JsonProperty("priority")
	private Priority priority;
	@JsonProperty("author")
	private Author author;
	@JsonProperty("subject")
	private String subject;
	@JsonProperty("description")
	private String description;
	@JsonProperty("start_date")
	private String startDate;
	@JsonProperty("done_ratio")
	private Integer doneRatio;
	@JsonProperty("created_on")
	private String createdOn;
	@JsonProperty("updated_on")
	private String updatedOn;
	@JsonProperty("notes")
	private Notes notes;
	@JsonProperty("due_date")
	private String dueDate;
	@JsonProperty("assigned_to")
	private User assignee;

	public User getAssignee() {
		return assignee;
	}

	public void setAssignee(User assignee) {
		this.assignee = assignee;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	@JsonProperty("notes")
	public Notes getNotes() {
		return notes;
	}

	@JsonProperty("notes")
	public void setNotes(Notes notes) {
		this.notes = notes;
	}

	@JsonProperty("id")
	public long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(long id) {
		this.id = id;
	}

	@JsonProperty("project")
	public Project getProject() {
		return project;
	}

	@JsonProperty("project")
	public void setProject(Project project) {
		this.project = project;
	}

	@JsonProperty("tracker")
	public Tracker getTracker() {
		return tracker;
	}

	@JsonProperty("tracker")
	public void setTracker(Tracker tracker) {
		this.tracker = tracker;
	}

	@JsonProperty("status")
	public Status getStatus() {
		return status;
	}

	@JsonProperty("status")
	public void setStatus(Status status) {
		this.status = status;
	}

	@JsonProperty("priority")
	public Priority getPriority() {
		return priority;
	}

	@JsonProperty("priority")
	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	@JsonProperty("author")
	public Author getAuthor() {
		return author;
	}

	@JsonProperty("author")
	public void setAuthor(Author author) {
		this.author = author;
	}

	@JsonProperty("subject")
	public String getSubject() {
		return subject;
	}

	@JsonProperty("subject")
	public void setSubject(String subject) {
		this.subject = subject;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("start_date")
	public String getStartDate() {
		return startDate;
	}

	@JsonProperty("start_date")
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	@JsonProperty("done_ratio")
	public Integer getDoneRatio() {
		return doneRatio;
	}

	@JsonProperty("done_ratio")
	public void setDoneRatio(Integer doneRatio) {
		this.doneRatio = doneRatio;
	}

	@JsonProperty("created_on")
	public String getCreatedOn() {
		return createdOn;
	}

	@JsonProperty("created_on")
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	@JsonProperty("updated_on")
	public String getUpdatedOn() {
		return updatedOn;
	}

	@JsonProperty("updated_on")
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}

}