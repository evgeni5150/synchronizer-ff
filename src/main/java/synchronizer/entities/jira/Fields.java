
package synchronizer.entities.jira;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "project",
    "summary",
    "issuetype",
    "assignee",
    "reporter",
    "priority",
    "labels",
    "timetracking",
    "security",
    "versions",
    "environment",
    "description",
    "duedate",
    "fixVersions",
    "components",
    "customfield_30000",
    "customfield_80000",
    "customfield_20000",
    "customfield_40000",
    "customfield_70000",
    "customfield_60000",
    "customfield_50000",
    "customfield_10000"
})
public class Fields {

    @JsonProperty("project")
    private Project project;
    @JsonProperty("summary")
    private String summary;
    @JsonProperty("issuetype")
    private Issuetype issuetype;
    @JsonProperty("assignee")
    private Assignee assignee;
    @JsonProperty("reporter")
    private Reporter reporter;
    @JsonProperty("priority")
    private Priority priority;
    @JsonProperty("labels")
    private List<String> labels = null;
    @JsonProperty("timetracking")
    private Timetracking timetracking;
    @JsonProperty("security")
    private Security security;
    @JsonProperty("versions")
    private List<Version> versions = null;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("description")
    private String description;
    @JsonProperty("duedate")
    private String duedate;
    @JsonProperty("fixVersions")
    private List<FixVersion> fixVersions = null;
    @JsonProperty("components")
    private List<Component> components = null;
    @JsonProperty("customfield_30000")
    private List<String> customfield30000 = null;
    @JsonProperty("customfield_80000")
    private Customfield80000 customfield80000;
    @JsonProperty("customfield_20000")
    private String customfield20000;
    @JsonProperty("customfield_40000")
    private String customfield40000;
    @JsonProperty("customfield_70000")
    private List<String> customfield70000 = null;
    @JsonProperty("customfield_60000")
    private String customfield60000;
    @JsonProperty("customfield_50000")
    private String customfield50000;
    @JsonProperty("customfield_10000")
    private String customfield10000;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("project")
    public Project getProject() {
        return project;
    }

    @JsonProperty("project")
    public void setProject(Project project) {
        this.project = project;
    }

    @JsonProperty("summary")
    public String getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(String summary) {
        this.summary = summary;
    }

    @JsonProperty("issuetype")
    public Issuetype getIssuetype() {
        return issuetype;
    }

    @JsonProperty("issuetype")
    public void setIssuetype(Issuetype issuetype) {
        this.issuetype = issuetype;
    }

    @JsonProperty("assignee")
    public Assignee getAssignee() {
        return assignee;
    }

    @JsonProperty("assignee")
    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    @JsonProperty("reporter")
    public Reporter getReporter() {
        return reporter;
    }

    @JsonProperty("reporter")
    public void setReporter(Reporter reporter) {
        this.reporter = reporter;
    }

    @JsonProperty("priority")
    public Priority getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @JsonProperty("labels")
    public List<String> getLabels() {
        return labels;
    }

    @JsonProperty("labels")
    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    @JsonProperty("timetracking")
    public Timetracking getTimetracking() {
        return timetracking;
    }

    @JsonProperty("timetracking")
    public void setTimetracking(Timetracking timetracking) {
        this.timetracking = timetracking;
    }

    @JsonProperty("security")
    public Security getSecurity() {
        return security;
    }

    @JsonProperty("security")
    public void setSecurity(Security security) {
        this.security = security;
    }

    @JsonProperty("versions")
    public List<Version> getVersions() {
        return versions;
    }

    @JsonProperty("versions")
    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    @JsonProperty("environment")
    public String getEnvironment() {
        return environment;
    }

    @JsonProperty("environment")
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("duedate")
    public String getDuedate() {
        return duedate;
    }

    @JsonProperty("duedate")
    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    @JsonProperty("fixVersions")
    public List<FixVersion> getFixVersions() {
        return fixVersions;
    }

    @JsonProperty("fixVersions")
    public void setFixVersions(List<FixVersion> fixVersions) {
        this.fixVersions = fixVersions;
    }

    @JsonProperty("components")
    public List<Component> getComponents() {
        return components;
    }

    @JsonProperty("components")
    public void setComponents(List<Component> components) {
        this.components = components;
    }

    @JsonProperty("customfield_30000")
    public List<String> getCustomfield30000() {
        return customfield30000;
    }

    @JsonProperty("customfield_30000")
    public void setCustomfield30000(List<String> customfield30000) {
        this.customfield30000 = customfield30000;
    }

    @JsonProperty("customfield_80000")
    public Customfield80000 getCustomfield80000() {
        return customfield80000;
    }

    @JsonProperty("customfield_80000")
    public void setCustomfield80000(Customfield80000 customfield80000) {
        this.customfield80000 = customfield80000;
    }

    @JsonProperty("customfield_20000")
    public String getCustomfield20000() {
        return customfield20000;
    }

    @JsonProperty("customfield_20000")
    public void setCustomfield20000(String customfield20000) {
        this.customfield20000 = customfield20000;
    }

    @JsonProperty("customfield_40000")
    public String getCustomfield40000() {
        return customfield40000;
    }

    @JsonProperty("customfield_40000")
    public void setCustomfield40000(String customfield40000) {
        this.customfield40000 = customfield40000;
    }

    @JsonProperty("customfield_70000")
    public List<String> getCustomfield70000() {
        return customfield70000;
    }

    @JsonProperty("customfield_70000")
    public void setCustomfield70000(List<String> customfield70000) {
        this.customfield70000 = customfield70000;
    }

    @JsonProperty("customfield_60000")
    public String getCustomfield60000() {
        return customfield60000;
    }

    @JsonProperty("customfield_60000")
    public void setCustomfield60000(String customfield60000) {
        this.customfield60000 = customfield60000;
    }

    @JsonProperty("customfield_50000")
    public String getCustomfield50000() {
        return customfield50000;
    }

    @JsonProperty("customfield_50000")
    public void setCustomfield50000(String customfield50000) {
        this.customfield50000 = customfield50000;
    }

    @JsonProperty("customfield_10000")
    public String getCustomfield10000() {
        return customfield10000;
    }

    @JsonProperty("customfield_10000")
    public void setCustomfield10000(String customfield10000) {
        this.customfield10000 = customfield10000;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
