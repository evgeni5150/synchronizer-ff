package synchronizer;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.client.task.ExternalTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import synchronizer.entities.jira.JiraBpmnTask;
import synchronizer.entities.jira.JiraResponse;
import synchronizer.entities.redmine.RedmineBpmnTask;
import synchronizer.entities.redmine.RedmineIssue;
import synchronizer.IssueTrackers;

@Component
public class TaskUtil {

	private static final Logger logger = LoggerFactory.getLogger(TaskUtil.class);
    
	@Autowired
	RestService restService;
	
	public URI generateUrl(IssueTrackers issueTracker, ExternalTask task) {
		String url = task.getVariable("trackerUrl").toString();
		String apiKey = task.getVariable("trackerApiKey").toString();
		url = url.endsWith("/") ? StringUtils.chop(url) : url;
		switch (issueTracker) {
		case JIRA:
			url = url + "/rest/api/2/issue/";
			break;
		case REDMINE:
			url = url + "/issues.json";
			break;
		}
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		builder = apiKey.length() > 0 ? builder.queryParam("key", apiKey) : builder;
		return builder.build().encode().toUri();
	}

	public String generateUrlForDiagramAsPng(String processDefinition, String camundaUrl) throws InterruptedException, IOException {
		String url = camundaUrl.endsWith("/") ? StringUtils.chop(camundaUrl) : camundaUrl;
		url = url + "/process-definition/key/" + processDefinition + "/diagram";
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		URI diagramUrl = builder.build().encode().toUri();
		byte[] imageByte = restService.getImageFromBpmnEngine(diagramUrl);
		String imgUrl = "";
		if(imageByte!=null) {
		imgUrl = ByteArrayToImage(imageByte);
		}
		return imgUrl;
	}

	public URI generateCamundaTaskCompleteUrl(String taskId, String camundaUrl) {
		String url = camundaUrl.endsWith("/") ? StringUtils.chop(camundaUrl) : camundaUrl;
		url = url + "/external-task/" + taskId + "/complete";
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
		return builder.build().encode().toUri();
	}

	public String generateBpmnTask(ExternalTask execution, IssueTrackers issueTracker, String camundaUrl)
			throws InterruptedException, IOException {

		// retrieve variables from the Workflow Engine
		String subject = "no subject";
		int projectId = 1;
		String description = "";
		int assigneeId = 0;
		String dueDate = "";
		ObjectMapper mapper = new ObjectMapper();
		String processImageUrl = generateUrlForDiagramAsPng(execution.getProcessDefinitionKey(), camundaUrl);
		

		if (execution.getVariable("preDefinedSubject") != null) {
			subject = execution.getVariable("preDefinedSubject").toString();

			// delete the predefined variable after been used to avoid ambiguity
			// execution.removeVariable("preDefinedSubject");
		} else if (execution.getVariable("subject") != null) {
			subject = execution.getVariable("subject").toString();
		}

		if (execution.getVariable("preDefinedProjectId") != null) {
			projectId = Integer.parseInt(execution.getVariable("preDefinedProjectId").toString());
			// execution.removeVariable("preDefinedProjectId");
		} else if (execution.getVariable("projectId") != null) {
			projectId = Integer.parseInt(execution.getVariable("projectId").toString());
		}
		if (execution.getVariable("preDefinedDescription") != null) {
			description = execution.getVariable("preDefinedDescription").toString();
			// execution.removeVariable("preDefinedDescription");
		} else if (execution.getVariable("description") != null) {
			description = execution.getVariable("description").toString();
		}
		if (execution.getVariable("preDefinedAssigneeId") != null) {
			assigneeId = Integer.parseInt(execution.getVariable("preDefinedAssigneeId").toString());
			// execution.removeVariable("preDefinedAssigneeId");
		} else if (execution.getVariable("assigneeId") != null) {
			assigneeId = Integer.parseInt(execution.getVariable("assigneeId").toString());
		}
		if (execution.getVariable("preDefinedDueDate") != null) {
			dueDate = execution.getVariable("preDefinedDueDate").toString();
			// execution.removeVariable("preDefinedDueDate");
		} else if (execution.getVariable("dueDate") != null) {
			dueDate = execution.getVariable("dueDate").toString();
		}
		if(!processImageUrl.equals("")) {
		description += "&nbsp; &nbsp; \"Diagram reference\":" + processImageUrl;
		}
		switch (issueTracker) {
		case JIRA: {
			JiraBpmnTask task = new JiraBpmnTask();
			task.setProjectId(projectId);
			task.setSubject(subject);
			task.setDescription(description);
			task.setAssigneeId(assigneeId);
			task.setProcessId(execution.getId());
			task.setDueDate(dueDate);
			return mapper.writeValueAsString(task);
		}
		case REDMINE: {
			RedmineBpmnTask task = new RedmineBpmnTask();
			task.setProjectId(projectId);
			task.setSubject(subject);
			task.setDescription(description);
			task.setAssigneeId(assigneeId);
			task.setProcessId(execution.getId());
			task.setDueDate(dueDate);
			task.setImageUrl(processImageUrl);
			return mapper.writeValueAsString(task);
		}
		default:
			return "";
		}
	}

	public RedmineIssue deserializeRedmineIssue(String issueJSON) throws JsonProcessingException {
		RedmineIssue redmineIssue = new RedmineIssue();
		ObjectMapper mapper = new ObjectMapper();
		try {
			redmineIssue = mapper.readValue(issueJSON, RedmineIssue.class);
		} catch (IOException e) {
			logger.error("Error deserializing redmine issue json.", e);
		}
		return redmineIssue;
	}

	public JiraResponse deserializeJiraIssue(String issueJSON) throws JsonProcessingException {
		JiraResponse jiraResponse = new JiraResponse();
		ObjectMapper mapper = new ObjectMapper();
		try {
			jiraResponse = mapper.readValue(issueJSON, JiraResponse.class);
		} catch (IOException e) {
			logger.error("Error deserializing jira issue json.", e);
		}
		return jiraResponse;
	}

	public String ByteArrayToImage(byte[] data) throws IOException {
		Date now = new Date();
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		BufferedImage image = ImageIO.read(bis);
		String imageName = "process_image_" + now.getTime();
		ImageIO.write(image, "png", new File("/var/www/img/" + imageName + ".png"));
		
		// TODO replace with actual url
		String url = "http://localhost:8090/images/" + imageName + ".png";
		return url;
	}
}
