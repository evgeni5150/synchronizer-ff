package synchronizer;

import java.io.IOException;
import java.net.URI;

import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.client.ExternalTaskClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.web.client.RestClientException;
import com.fasterxml.jackson.databind.ObjectMapper;
import synchronizer.entities.ExternalReference;
import synchronizer.entities.redmine.RedmineIssue;

@EnableRetry
@SpringBootApplication
public class WebService {

	private static final Logger logger = LoggerFactory.getLogger(WebService.class);

	public static void main(String... args) throws InterruptedException {
		SpringApplication.run(WebService.class, args);
	}

	@Autowired
	private ExternalReferenceRepository repository;

	@Autowired
	private TaskUtil taskUtil;

	@Autowired
	private RestService restService;
	
	// TODO Remove this and implement subscription trough modeler plugin
	@Value("${CAMUNDA_URL}")
	private String CAMUNDA_URL;


	@EventListener(ApplicationReadyEvent.class)
	public void runTheWorker() {
		ObjectMapper mapper = new ObjectMapper();
		String synchronizerTaskWorkerId = "synchronizerTaskWorkerId";
		String url = CAMUNDA_URL.endsWith("/") ? StringUtils.chop(CAMUNDA_URL) : CAMUNDA_URL;
		ExternalTaskClient client = ExternalTaskClient.create().baseUrl(url)
				.workerId(synchronizerTaskWorkerId).build(); // TODO introduce BackOff strategy

		// subscribe to the topic
		client.subscribe("synchronizedTask").handler((externalTask, externalTaskService) -> {
			logger.info("Activity with id " + externalTask.getActivityId()
					+ " fetched and locked by external worker named: " + synchronizerTaskWorkerId);
			String executionId = externalTask.getExecutionId();
			ExternalReference ref = repository.findByExecutionId(executionId);
			if (ref != null) {
				// stop the worker if issue is already sent to Issue Tracker
				return;
			}
			String camundaUrl = externalTask.getVariable("camundaUrl").toString();
			String activityId = externalTask.getActivityId();
			RedmineIssue issue = new RedmineIssue();
			int issueTrackerId = 1;
			if (externalTask.getVariable("preDefinedIssueTracker") != null) {
				issueTrackerId = Integer.parseInt(externalTask.getVariable("preDefinedIssueTracker").toString());
			} else if (externalTask.getVariable("issueTracker") != null) {
				issueTrackerId = Integer.parseInt(externalTask.getVariable("issueTracker").toString());
			}
			IssueTrackers issueTracker = IssueTrackers.get(issueTrackerId);
			String taskJSON = "";

			try {
				
				taskJSON = taskUtil.generateBpmnTask(externalTask, issueTracker, url);
			} catch (IOException | InterruptedException e1) {
				e1.printStackTrace();
			}

			try {
				URI trackerUrl = taskUtil.generateUrl(issueTracker, externalTask);
				String result = restService.sendIssue(taskJSON, trackerUrl, "Issue Tracker");
				issue = mapper.readValue(result, RedmineIssue.class);

			} catch (RestClientException t) {
				logger.warn("Cannot create issue for task with id: " + activityId + ". Issue tracker is unreachable.");
				// TODO enable notification service and setup backup procedure

				// taskRepository.save(task); TODO store the task only when all attempts failed
				return;

			} catch (IOException | InterruptedException e) {
				externalTaskService.handleBpmnError(externalTask,
						"Error in issue tracker's synchronization web service.");
				return;
			}
			logger.info("Issue with ID: " + issue.getId() + " has been sent!");
			ExternalReference r = new ExternalReference();
			r.setIssueId(issue.getId());
			r.setExecutionId(externalTask.getExecutionId());
			r.setTaskId(externalTask.getId());
			r.setTaskWorkerId(synchronizerTaskWorkerId);
			r.setCamundaUrl(camundaUrl);
			repository.save(r);

		}).open();
	}

}
