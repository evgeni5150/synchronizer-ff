package synchronizer;

import org.springframework.data.mongodb.repository.MongoRepository;

import synchronizer.entities.BpmnTask;

public interface ExternalTaskRepository extends MongoRepository<BpmnTask, String> {

}
