'use strict';

module.exports = function(electronApp, menuState) {
 return [
{
    label: 'Synchronize!',
    enabled: function() {
      return true;
    },
    action: function() {
      const {BrowserWindow, app, ipcMain} = require('electron')
      let win = new BrowserWindow({width: 800, height: 600})

      win.on('closed', () => {
        win = null
      })
      // Or load a local HTML file
      win.loadURL(`file://${__dirname}/form.html`)
      ipcMain.on("changeWindow", function(event, json) {
        var fs = require('fs');
        var t = app.getAppPath()
        var path = t.substr(0, t.lastIndexOf("/"));
            if (!fs.existsSync(path+'/element-templates')) {
              fs.mkdir(path+'/element-templates');
              }
              fs.writeFileSync(path+'/element-templates/synced.json', json, 'utf-8');
           
       });

    }
}

];
};
