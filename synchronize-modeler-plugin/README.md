# Camunda Modeler Plugin For Synchronization with Issue Tracker

This plugin allows camunda modeler users to synchronize Service Task's with Issue Trackers. 

## Issue Trackers available

 - Redmine
 - Jira
